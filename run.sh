#!/bin/bash

set -e
set -o pipefail

ARG1="$1"
GENERATIONS="1"
HIDDENNODES="17"
HIDDENLAYERS="1"
STDDEV="0.02"
INITIALCT="500"
REDUCECT="100"
CHILDRENCT="4"
TRAININGDIR="training_paypal"
OUTPUTPATH="${TRAININGDIR}_${HIDDENNODES}x${HIDDENLAYERS}_gen${GENERATIONS}_std${STDDEV}_init${INITIALCT}_red${REDUCECT}_chd${CHILDRENCT}"

if [[ $ARG1 -eq "-p" ]] ; then
    valgrind --tool=callgrind --callgrind-out-file=./callgrind.profile ./cbrain -g 100 -t 1.0 -i ./xor_training/input -o ./xor_training/output -s 2 -S 1 -h 3 -H 3 -d 0.05 -O profile_xor.ffnet -I 1000 -r 50 -c 20
else
    ./cbrain -g $GENERATIONS -t 0.25 -i ./training_paypal/pos_input -o ./training_paypal/pos_output -i ./training_paypal/neg_input -o ./training_paypal/neg_output -s 288 -S 1 -h $HIDDENNODES -H $HIDDENLAYERS -d $STDDEV -O trained_${OUTPUTPATH}.ffnet -I $INITIALCT -r $REDUCECT -c $CHILDRENCT > results_${OUTPUTPATH}.txt
    echo "Wrote to results_${OUTPUTPATH}.txt"
fi
