#pragma once
#include <memory>
#include <vector>
#include <string>
#include "cbrain.pb.h"
#include "random.h"

class Neuron;

class Synapse {
    public:
        void connect(Neuron&, double);
        void set_weight(double);
        void mutate(RandomNumberGenerator*);
        std::string serialize();
        double weight;
        int dest;
        cbrain::Synapse pb_serialize();
};

typedef std::vector<Synapse> SynapseV;

class Neuron {
    public:
        void connect(Neuron&, double);
        double get_tanh_signal();
        void mutate(RandomNumberGenerator*);
        std::string serialize();
        void debug();
        int index;
        double signal;
        SynapseV synapses;
        cbrain::Neuron pb_serialize();
};
