project(CBRAIN)

#MESSAGE(STATUS "CBD: " ${CMAKE_CURRENT_BINARY_DIR})

# source filenames
set(BRAIN_HS neural_net.h neuron.h genetic.h random.h train.h datadir.h cbrain.pb.h)
set(BRAIN_CPPS neural_net.cpp neuron.cpp genetic.cpp random.cpp train.cpp datadir.cpp cbrain.pb.cc)

# make
add_library(brain SHARED ${BRAIN_HS} ${BRAIN_CPPS})
add_executable(cbrain cbrain.cpp cbrain.h)
target_link_libraries(cbrain brain)

# pthreads
find_package(Threads REQUIRED)
if(THREADS_HAVE_PTHREAD_ARG)
    target_compile_options(PUBLIC cbrain "-pthread")
endif()
if(CMAKE_THREAD_LIBS_INIT)
    target_link_libraries(cbrain "${CMAKE_THREAD_LIBS_INIT} -lprotobuf")
endif()

# make install
install(TARGETS brain
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
)
install(FILES ${BRAIN_HS} DESTINATION include/brain)
