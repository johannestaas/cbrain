#include <memory>
#include <iostream>
#include <cmath>
#include <string>
#include "cbrain.pb.h"
#include "neuron.h"
#include "random.h"

std::string Synapse::serialize() {
    std::string s;
    s.append("synapse ");
    s.append(std::to_string(weight));
    s.append(" ");
    s.append(std::to_string(dest));
    return s;
}

std::string Neuron::serialize() {
    std::string s;
    s.append("neuron ");
    s.append(std::to_string(index));
    s.append("\n");
    for (auto &syn : synapses) {
        s.append(syn.serialize());
        s.append("\n");
    }
    return s;
}

void Synapse::set_weight(double d) {
    weight = d;
}

void Synapse::connect(Neuron &other, double wt) {
    dest = other.index;
    weight = wt;
}

void Synapse::mutate(RandomNumberGenerator *rng) {
    auto d = rng->normal();
    // std::cout << "rng.normal: " << d << std::endl;
    weight += d;
}

void Neuron::connect(Neuron &other, double wt) {
    auto s = Synapse();
    s.connect(other, wt);
    synapses.push_back(s);
}

double Neuron::get_tanh_signal() {
    return tanh(signal + 1.0);
}

void Neuron::mutate(RandomNumberGenerator *rng) {
    for (auto &s : synapses) {
        s.mutate(rng);
    }
}

void Neuron::debug() {
    for (auto &s : synapses) {
        std::cout << "    - Synapse weight=" << s.weight << std::endl;
    }
}

cbrain::Synapse Synapse::pb_serialize() {
    cbrain::Synapse s = cbrain::Synapse();
    s.set_weight(weight);
    s.set_dest(dest);
    return s;
}

cbrain::Neuron Neuron::pb_serialize() {
    cbrain::Neuron n = cbrain::Neuron();
    n.set_index(index);
    n.set_signal(signal);
    for (auto &syn : synapses) {
        cbrain::Synapse *s = n.add_synapses();
        s->set_weight(syn.weight);
        s->set_dest(syn.dest);
    }
    return n;
}
