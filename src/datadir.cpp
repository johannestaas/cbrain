#include <vector>
#include <string>
#include <dirent.h>
#include "datadir.h"

DataDir::DataDir(std::string dir_path0) {
    DIR *dpdf;
    struct dirent *epdf;
    dir_path = dir_path0;

    dpdf = opendir(dir_path.c_str());
    if (dpdf != NULL) {
        while (true) {
            epdf = readdir(dpdf);
            if (epdf == NULL) {
                break;
            }
            if (sizeof(epdf->d_name) > 0) {
                auto d_name = std::string(epdf->d_name);
                if ((d_name.compare(".") == 0) || (d_name.compare("..") == 0)) {
                    continue;
                }
                contents.push_back(d_name);
            }
        }
    }
    const_iterator = contents.cbegin();
}
