#include <iostream>
#include <memory>
#include <thread>
#include <deque>
#include <string>
#include "train.h"
#include "neural_net.h"
#include "random.h"

typedef std::deque<FFNet> FFNetQ;
typedef TrainingData Data;

class GeneticGenerator {
    public:
        void set_training_data(Data);
        void set_hidden_size(int ct, int layer_ct);
        void train(int generations, int initial_ct, int sample_ct, int children_ct, double stddev, std::string output_path);
        FFNet new_net();
        void run_generation();
        void output_results(FFNet&);
        void setup_rng(double);
    private:
        Data data;
        FFNetQ nets;
        RandomNumberGenerator rng;
        int hidden_ct;
        int hidden_layer_ct;
};
