#include <fstream>
#include <memory>
#include <vector>
#include <iostream>
#include <string>
#include "cbrain.pb.h"
#include "neural_net.h"
#include "neuron.h"
#include "random.h"

FFNet::FFNet(RandomNumberGenerator *prng) {
    rng = prng;
}

FFNet::~FFNet() {
    input_layer.clear();
    layers.clear();
    output_layer.clear();
}

FFNet::FFNet(const FFNet& other) {
    rng = other.get_rng();
    error = other.get_error();
    input_layer = NeuronV(other.get_input_layer());
    layers = NeuronVV(other.get_layers());
    output_layer = NeuronV(other.get_output_layer());
    false_positives = other.false_positives;
    true_positives = other.true_positives;
    false_negatives = other.false_negatives;
    true_negatives = other.true_negatives;
}

FFNet& FFNet::operator=(const FFNet& other) {
    rng = other.get_rng();
    error = other.get_error();
    input_layer = NeuronV(other.get_input_layer());
    layers = NeuronVV(other.get_layers());
    output_layer = NeuronV(other.get_output_layer());
    false_positives = other.false_positives;
    true_positives = other.true_positives;
    false_negatives = other.false_negatives;
    true_negatives = other.true_negatives;
    return *this;
}

bool FFNet::operator<(const FFNet& o) const {
    int my_false = 10 * false_positives + 7 * false_negatives;
    int o_false = 10 * o.false_positives + 7 * o.false_negatives;
    if (my_false < o_false) {
        return true;
    } else if (o_false < my_false) {
        return false;
    }
    // If they're equal, favor the least false positives.
    if (false_positives < o.false_positives) {
        return true;
    }
    // If they're the same, then use error to get it close to 1.0 and -1.0.
    return error < o.get_error();
}
    
void FFNet::add_input_layer(unsigned int ct) {
    for (unsigned int i=0; i<ct; i++) {
        Neuron n;
        n.index = i;
        input_layer.push_back(n);
    }
}

void FFNet::add_hidden_layers(unsigned int ct, unsigned int layer_ct) {
    NeuronV* last_layer = &input_layer;
    for (unsigned int i=0; i<layer_ct; i++) {
        NeuronV layer = NeuronV();
        for (unsigned int n_i=0; n_i<ct; n_i++) {
            Neuron n;
            n.index = n_i;
            layer.push_back(n);
            for (auto &ln : *last_layer) {
                if (rng != NULL) {
                    double wt = rng->uniform();
                    ln.connect(n, wt);
                } else {
                    ln.connect(n, 0.0);
                }
            }
        }
        layers.push_back(layer);
        last_layer = &layers.back();
    }
}

void FFNet::add_output_layer(unsigned int ct) {
    for (unsigned int i=0; i<ct; i++) {
        Neuron n;
        n.index = i;
        for (auto &ln : layers.back()) {
            if (rng != NULL) {
                double wt = rng->uniform();
                ln.connect(n, wt);
            } else {
                ln.connect(n, 0.0);
            }
        }
        output_layer.push_back(n);
    }
}

void FFNet::mutate() {
    for (auto &n : input_layer) {
        n.mutate(rng);
    }
    for (auto &layer : layers) {
        for (auto &n : layer) {
            n.mutate(rng);
        }
    }
    for (auto &n : output_layer) {
        n.mutate(rng);
    }
}

void FFNet::activate(DoubleV *inputs) {
    //debug();
    int i = 0;
    for (auto &n : input_layer) {
        n.signal = inputs->at(i);
        i++;
    }
    for (auto &n : input_layer) {
        double sig = n.signal;
        for (auto &s : n.synapses) {
            int i = s.dest;
            double wt = s.weight;
            layers.front().at(i).signal += sig * wt;
        }
    }
    for (int layer_i=0; layer_i<(int)layers.size(); layer_i++) {
        NeuronV* next_layer = &output_layer;
        NeuronV* layer = &layers.at(layer_i);
        if ((layer_i + 1) == (int)layers.size()) {
            next_layer = &output_layer;
        } else {
            next_layer = &layers.at(layer_i + 1);
        }
        for (auto &n : *layer) {
            double sig = n.get_tanh_signal();
            for (auto &s : n.synapses) {
                int s_dest = s.dest;
                double wt = s.weight;
                next_layer->at(s_dest).signal += sig * wt;
            }
        }
    }
}

void FFNet::get_output_into(double outputs[]) {
    int i = 0;
    for (auto &n : output_layer) {
        outputs[i] = n.signal;
        i++;
    }
}

void FFNet::get_output_into(DoubleV &outputs) {
    for (auto &n : output_layer) {
        outputs.push_back(n.signal);
    }
}

DoubleV FFNet::get_output() {
    DoubleV vec;
    for (auto &n : output_layer) {
        vec.push_back(n.signal);
    }
    return vec;
}

DoubleV FFNet::get_output_sign() {
    DoubleV vec;
    for (auto &n : output_layer) {
        vec.push_back(n.signal);
        if (n.signal < 0) {
            vec.push_back(0.0);
        } else {
            vec.push_back(1.0);
        }
    }
    return vec;
}

void FFNet::reset() {
    for (auto &layer : layers) {
        for (auto &n : layer) {
            n.signal = 0.0;
        }
    }
    for (auto &n : output_layer) {
        n.signal = 0.0;
    }
}

void FFNet::set_error(double err) {
    error = err;
}

double FFNet::get_error() const {
    return error;
}

const NeuronV& FFNet::get_input_layer() const {
    return input_layer;
}

const NeuronVV& FFNet::get_layers() const {
    return layers;
}

const NeuronV& FFNet::get_output_layer() const {
    return output_layer;
}

std::string FFNet::serialize() {
    std::string s;
    s.append("FFNet\n");
    s.append("input_layer\n");
    for (auto &n : input_layer) {
        s.append(n.serialize());
    }
    s.append("layers\n");
    int i = 0;
    for (auto &l : layers) {
        s.append("layer ");
        s.append(std::to_string(i));
        s.append("\n");
        for (auto &n : l) {
            s.append(n.serialize());
        }
        i++;
    }
    s.append("output_layer\n");
    for (auto &n : output_layer) {
        s.append(n.serialize());
    }
    return s;
}

void FFNet::write(std::string path) {
    std::ofstream output_f;
    output_f.open(path);
    auto ffn = pb_serialize();
    if (!ffn.SerializeToOstream(&output_f)) {
        std::cerr << "Failed to save to " << path << std::endl;
    }
}

void FFNet::debug() {
    int i = 0;
    std::cout << "Input Layer" << std::endl;
    for (auto &n : input_layer) {
        std::cout << "  - Neuron " << i << std::endl;
        n.debug();
        i++;
    }
    int l_i = 0;
    for (auto &v : layers) {
        std::cout << "Hidden Layer " << l_i << std::endl;
        int i = 0;
        for (auto &n : v) {
            std::cout << "  - Neuron " << i << std::endl;
            n.debug();
            i++;
        }
        l_i++;
    }
    i = 0;
    std::cout << "Output Layer" << std::endl;
    for (auto &n : output_layer) {
        std::cout << "  - Neuron " << i << std::endl;
        n.debug();
        i++;
    }
}

double FFNet::calc_accuracy() const {
    double nom = double(true_positives + true_negatives);
    double denom = double(true_positives + true_negatives + false_positives + false_negatives);
    if (denom == 0.0) {
        return 0.0;
    }
    return nom / denom;
}

double FFNet::calc_precision() const {
    double nom = double(true_positives);
    double denom = double(true_positives + false_positives);
    if (denom == 0.0) {
        return 0.0;
    }
    return nom / denom;
}

double FFNet::calc_sensitivity() const {
    double nom = double(true_positives);
    double denom = double(true_positives + false_negatives);
    if (denom == 0.0) {
        return 0.0;
    }
    return nom / denom;
}

double FFNet::calc_specificity() const {
    double nom = double(true_negatives);
    double denom = double(true_negatives + false_positives);
    if (denom == 0.0) {
        return 0.0;
    }
    return nom / denom;
}

void FFNet::reset_measurements() {
    false_positives = 0;
    false_negatives = 0;
    true_positives = 0;
    true_negatives = 0;
}

void FFNet::print_measurements() {
    std::cout << "TP: " << true_positives << " FP: " << false_positives
                        << std::endl << "TN: " << true_negatives 
                        << " FN: " << false_negatives << std::endl;
    std::cout << "Accuracy: " << calc_accuracy() << std::endl;
    std::cout << "Precision: " << calc_precision() << std::endl;
    std::cout << "Specificity: " << calc_specificity() << std::endl;
    std::cout << "Sensitivity: " << calc_sensitivity() << std::endl;
}

cbrain::FeedForwardNeuralNet FFNet::pb_serialize() {
    cbrain::FeedForwardNeuralNet n;
    n.set_input_layer_size(input_layer.size());
    n.set_hidden_layer_size(layers.at(0).size());
    n.set_hidden_layer_ct(layers.size());
    n.set_output_layer_size(output_layer.size());
    n.set_stddev(rng->stddev);
    cbrain::NeuralNetLayer *ilayer = n.mutable_input_layer();   
    for (auto &nr : input_layer) {
        cbrain::Neuron *neuron = ilayer->add_neurons();
        neuron->set_index(nr.index);
        neuron->set_signal(nr.signal);
        for (auto &s : nr.synapses) {
            cbrain::Synapse *syn = neuron->add_synapses();
            syn->set_weight(s.weight);
            syn->set_dest(s.dest);
        }
    }
    for (auto &layer : layers) {
        cbrain::NeuralNetLayer *hlayer = n.add_layers();   
        for (auto &nr : layer) {
            cbrain::Neuron *neuron = hlayer->add_neurons();
            neuron->set_index(nr.index);
            neuron->set_signal(nr.signal);
            for (auto &s : nr.synapses) {
                cbrain::Synapse *syn = neuron->add_synapses();
                syn->set_weight(s.weight);
                syn->set_dest(s.dest);
            }
        }
    }
    cbrain::NeuralNetLayer *olayer = n.mutable_output_layer();   
    for (auto &nr : output_layer) {
        cbrain::Neuron *neuron = olayer->add_neurons();
        neuron->set_index(nr.index);
        neuron->set_signal(nr.signal);
        for (auto &s : nr.synapses) {
            cbrain::Synapse *syn = neuron->add_synapses();
            syn->set_weight(s.weight);
            syn->set_dest(s.dest);
        }
    }
    n.set_error(error);
    return n;
}

void FFNet::pb_serialize(cbrain::FeedForwardNeuralNet *pb_net) {
    pb_net->set_input_layer_size(input_layer.size());
    pb_net->set_hidden_layer_size(layers.at(0).size());
    pb_net->set_hidden_layer_ct(layers.size());
    pb_net->set_output_layer_size(output_layer.size());
    pb_net->set_stddev(rng->stddev);
    cbrain::NeuralNetLayer *ilayer = pb_net->mutable_input_layer();   
    for (auto &nr : input_layer) {
        cbrain::Neuron *neuron = ilayer->add_neurons();
        neuron->set_index(nr.index);
        neuron->set_signal(nr.signal);
        for (auto &s : nr.synapses) {
            cbrain::Synapse *syn = neuron->add_synapses();
            syn->set_weight(s.weight);
            syn->set_dest(s.dest);
        }
    }
    for (auto &layer : layers) {
        cbrain::NeuralNetLayer *hlayer = pb_net->add_layers();   
        for (auto &nr : layer) {
            cbrain::Neuron *neuron = hlayer->add_neurons();
            neuron->set_index(nr.index);
            neuron->set_signal(nr.signal);
            for (auto &s : nr.synapses) {
                cbrain::Synapse *syn = neuron->add_synapses();
                syn->set_weight(s.weight);
                syn->set_dest(s.dest);
            }
        }
    }
    cbrain::NeuralNetLayer *olayer = pb_net->mutable_output_layer();   
    for (auto &nr : output_layer) {
        cbrain::Neuron *neuron = olayer->add_neurons();
        neuron->set_index(nr.index);
        neuron->set_signal(nr.signal);
        for (auto &s : nr.synapses) {
            cbrain::Synapse *syn = neuron->add_synapses();
            syn->set_weight(s.weight);
            syn->set_dest(s.dest);
        }
    }
    pb_net->set_error(error);
}

FFNet& FFNet::operator=(const cbrain::FeedForwardNeuralNet &pb_net) {
    rng = NULL;
    add_input_layer(pb_net.input_layer().neurons_size());
    add_hidden_layers(pb_net.layers(0).neurons().size(), pb_net.layers_size());
    add_output_layer(pb_net.output_layer().neurons_size());
    error = pb_net.error();
    auto &il = pb_net.input_layer();
    for (int i=0; i<il.neurons_size(); i++) {
        auto &pb_n = il.neurons(i);
        auto &n = input_layer.at(i);
        n.index = pb_n.index();
        n.signal = pb_n.signal();
        for (int j=0; j<pb_n.synapses_size(); j++) {
            auto &pb_s = pb_n.synapses(j);
            auto &s = n.synapses.at(j);
            s.weight = pb_s.weight();
            s.dest = pb_s.dest();
        }
    }
    for (int l_i=0; l_i<pb_net.layers_size(); l_i++) {
        auto &il = pb_net.layers(l_i);
        auto &layer = layers.at(l_i);
        for (int i=0; i<il.neurons_size(); i++) {
            auto &pb_n = il.neurons(i);
            auto &n = layer.at(i);
            n.index = pb_n.index();
            n.signal = pb_n.signal();
            for (int j=0; j<pb_n.synapses_size(); j++) {
                auto &pb_s = pb_n.synapses(j);
                auto &s = n.synapses.at(j);
                s.weight = pb_s.weight();
                s.dest = pb_s.dest();
            }
        }
    }
    auto &ol = pb_net.output_layer();
    for (int i=0; i<ol.neurons_size(); i++) {
        auto &pb_n = ol.neurons(i);
        auto &n = output_layer.at(i);
        n.index = pb_n.index();
        n.signal = pb_n.signal();
        for (int j=0; j<pb_n.synapses_size(); j++) {
            auto &pb_s = pb_n.synapses(j);
            auto &s = n.synapses.at(j);
            s.weight = pb_s.weight();
            s.dest = pb_s.dest();
        }
    }
    return *this;
}

FFNet::FFNet(const cbrain::FeedForwardNeuralNet &pb_net) {
    *this = pb_net;
}
