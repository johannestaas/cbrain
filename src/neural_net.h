#pragma once
#include <memory>
#include <vector>
#include <deque>
#include <string>
#include "cbrain.pb.h"
#include "neuron.h"
#include "random.h"

typedef std::vector<double> DoubleV;
typedef std::deque<Neuron> NeuronQ;
typedef std::vector<Neuron> NeuronV;
typedef std::vector<NeuronV> NeuronVV;

class FFNet {
    public:
        FFNet(RandomNumberGenerator *prng);
        FFNet(const cbrain::FeedForwardNeuralNet&);
        ~FFNet();
        FFNet(const FFNet&);
        FFNet& operator=(const FFNet&);
        FFNet& operator=(const cbrain::FeedForwardNeuralNet&);
        bool operator<(const FFNet &a) const;
        void add_input_layer(unsigned int);
        void add_hidden_layers(unsigned int, unsigned int);
        void add_output_layer(unsigned int);
        void activate(double[]);
        void activate(DoubleV*);
        void reset();
        void reset_measurements();
        void mutate();
        void get_output_into(double[]);
        void get_output_into(DoubleV&);
        DoubleV get_output();
        DoubleV get_output_sign();
        void set_error(double);
        double get_error() const;
        const NeuronV& get_input_layer() const;
        const NeuronVV& get_layers() const;
        const NeuronV& get_output_layer() const;
        std::string serialize();
        void write(std::string);
        void set_rng(RandomNumberGenerator prng) { rng = &prng; };
        RandomNumberGenerator* get_rng() const { return rng; };
        void debug();
        void print_measurements();
        double calc_accuracy() const;
        double calc_precision() const;
        double calc_sensitivity() const;
        double calc_specificity() const;
        int false_positives;
        int false_negatives;
        int true_positives;
        int true_negatives;
        cbrain::FeedForwardNeuralNet pb_serialize();
        void pb_serialize(cbrain::FeedForwardNeuralNet*);
        RandomNumberGenerator *rng;
    private:
        NeuronV input_layer;
        NeuronVV layers;
        NeuronV output_layer;
        double error;
};
