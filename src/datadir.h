#pragma once
#include <vector>
#include <string>

class DataDir {
    public:
        DataDir(std::string);
        ~DataDir() = default;
        DataDir(const DataDir&) = default;
        DataDir& operator=(const DataDir&) = default;
        std::vector<std::string>::const_iterator const_iterator;
        std::vector<std::string>::const_iterator begin() const { return contents.begin(); };
        std::vector<std::string>::const_iterator end() const { return contents.end(); };
        std::string& at(int i) { return contents.at(i); };
        void clear() { contents.clear(); };
        size_t size() { return contents.size(); };
    private:
        std::string dir_path;
        std::vector<std::string> contents;
};
