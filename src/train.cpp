#include <fstream>
#include <iostream>
#include <cmath>
#include <memory>
#include <vector>
#include <string>
#include <algorithm>
#include "train.h"
#include "datadir.h"

void TrainingData::set_training_sample(IOPathPairV ts) {
    training_sample = ts;
}

void TrainingData::set_fitness_sample(IOPathPairV fs) {
    fitness_sample = fs;
}

IOPathPairV TrainingData::scan_dirs(std::string input_dir, std::string output_dir) {
    DataDir dd = DataDir(input_dir);
    IOPathPairV path_pairs;
    for (auto &fn : dd) {
        std::string input_path, output_path;
        input_path.append(input_dir);
        input_path.append("/");
        input_path.append(fn);
        output_path.append(output_dir);
        output_path.append("/");
        output_path.append(fn);
        auto path_pair = IOPathPair(input_path, output_path);
        path_pairs.push_back(path_pair);
    }
    std::cout << "Found " << path_pairs.size() << " pairs in " << input_dir << std::endl;
    return path_pairs;
}

void TrainingData::randomize_samples(IOPathPairV pairs, float training_ratio) {
    float total_size = float(pairs.size());
    std::random_shuffle(pairs.begin(), pairs.end());
    int training_size = int(training_ratio * total_size);
    int fitness_size = int(pairs.size()) - training_size;
    for (int i=0; i<training_size; i++) {
        training_sample.push_back(pairs.back());
        pairs.pop_back();
    }
    while (!pairs.empty()) {
        fitness_sample.push_back(pairs.back());
        pairs.pop_back();
    }
    std::cout << training_size << " I/O pairs will be used to train." << std::endl;
    std::cout << fitness_size << " I/O pairs will test the fitness of the neural net."
              << std::endl;
}

void TrainingData::preload_training_samples() {
    cached_input = NamedIOPairV();
    for (auto &path_pair : training_sample) {
        auto input_path = path_pair.first;
        auto output_path = path_pair.second;
        std::vector<double> input_doubles;
        std::vector<double> output_doubles;
        std::ifstream in_file;
        std::ifstream out_file;
        in_file.open(input_path);
        out_file.open(output_path);
        for (unsigned int i=0; i<in_size; i++) {
            double d;
            in_file >> d;
            input_doubles.push_back(d);
        }
        for (unsigned int i=0; i<out_size; i++) {
            double d;
            out_file >> d;
            output_doubles.push_back(d);
        }
        auto io_pair = IOVPair(input_doubles, output_doubles);
        auto named_io_pair = NamedIOPair(input_path, io_pair);
        cached_input.push_back(named_io_pair);
    }
}

NamedIOPair TrainingData::next_training_sample() {
    auto path_pair = training_sample.at(training_index);
    training_index++;
    auto input_path = path_pair.first;
    auto output_path = path_pair.second;
    std::vector<double> input_doubles;
    std::vector<double> output_doubles;
    std::ifstream in_file;
    std::ifstream out_file;
    in_file.open(input_path);
    out_file.open(output_path);
    for (unsigned int i=0; i<in_size; i++) {
        double d;
        in_file >> d;
        input_doubles.push_back(d);
    }
    for (unsigned int i=0; i<out_size; i++) {
        double d;
        out_file >> d;
        output_doubles.push_back(d);
    }
    auto io_pair = IOVPair(input_doubles, output_doubles);
    return NamedIOPair(input_path, io_pair);
}

NamedIOPair TrainingData::next_fitness_sample() {
    auto path_pair = fitness_sample.at(fitness_index);
    fitness_index++;
    auto input_path = path_pair.first;
    auto output_path = path_pair.second;
    std::vector<double> input_doubles;
    std::vector<double> output_doubles;
    std::ifstream in_file;
    std::ifstream out_file;
    in_file.open(input_path);
    out_file.open(output_path);
    for (unsigned int i=0; i<in_size; i++) {
        double d;
        in_file >> d;
        input_doubles.push_back(d);
    }
    for (unsigned int i=0; i<out_size; i++) {
        double d;
        out_file >> d;
        output_doubles.push_back(d);
    }
    auto io_pair = IOVPair(input_doubles, output_doubles);
    return NamedIOPair(input_path, io_pair);
}

void TrainingData::set_sizes(unsigned int input_ct, unsigned int output_ct) {
    in_size = input_ct;
    out_size = output_ct;
    std::cout << "Created training data of input/output " << in_size << "/"
              << out_size << std::endl;
}

void TrainingData::add_pair(double inputs[], double outputs[]) {
    IOPair pair;
    std::vector<double> in_vec = std::vector<double>(inputs, inputs + in_size);
    std::vector<double> out_vec = std::vector<double>(outputs, outputs + out_size);
    pair.push_back(in_vec);
    pair.push_back(out_vec);
    io_pairs.push_back(pair);
}

unsigned int TrainingData::get_input_size() {
    return in_size;
}

unsigned int TrainingData::get_output_size() {
    return out_size;
}

IOPair TrainingData::get_pair(unsigned int i) {
    return io_pairs.at(i);
}

std::vector<double>* TrainingData::get_inputs(unsigned int i) {
    return &io_pairs.at(i).at(0);
}

std::vector<double>& TrainingData::get_outputs(unsigned int i) {
    return io_pairs.at(i).at(1);
}

unsigned int TrainingData::get_pair_size() {
    return io_pairs.size();
}

double TrainingData::calc_error(unsigned int i, std::vector<double> &outputs) {
    auto good_outputs = get_outputs(i);
    double err = 0.0;
    for (unsigned int i=0; i<out_size; i++) {
        double erred = pow(fabs(good_outputs.at(i) - outputs.at(i)) + 1.0, 2.0);
        err += erred;
    }
    return err;
}

double TrainingData::calc_error(std::vector<double> &good_outputs, std::vector<double> &outputs) {
    double err = 0.0;
    for (unsigned int i=0; i<out_size; i++) {
        double erred = pow(fabs(good_outputs.at(i) - outputs.at(i)) + 1.0, 2.0);
        // double erred = fabs(good_outputs.at(i) - outputs.at(i));
        err += erred;
    }
    return err;
}

void TrainingData::set_measurements(FFNet *net, std::vector<double> &good_outputs, std::vector<double> &outputs) {
    for (unsigned int i=0; i<out_size; i++) {
        if (good_outputs.at(i) < 0) {
            if (outputs.at(i) < 0) {
                net->true_negatives++;
            } else {
                net->false_positives++;
            }
        } else {
            if (outputs.at(i) < 0) {
                net->false_negatives++;
            } else {
                net->true_positives++;
            }
        }
    }
}

double TrainingData::calc_error_final(std::vector<double> &good_outputs, std::vector<double> &outputs) {
    double err = 0.0;
    for (unsigned int i=0; i<out_size; i++) {
        double erred = fabs(good_outputs.at(i) - outputs.at(i));
        err += erred;
    }
    return err;
}

double TrainingData::calc_error(std::vector<double> &errors) {
    double err = 0.0;
    for (auto d : errors) {
        err += d;
    }
    return err;
}

double TrainingData::calc_error_final(std::vector<double> &errors) {
    double err = 0.0;
    for (auto d : errors) {
        err += d;
    }
    return err;
}
