#include <random>
#include "random.h"

RandomNumberGenerator::RandomNumberGenerator() {}

RandomNumberGenerator::~RandomNumberGenerator() {}

double RandomNumberGenerator::uniform() {
    return uniform_dist(generator);
}

double RandomNumberGenerator::normal() {
    return normal_dist(generator);
}

void RandomNumberGenerator::setup_normal_dist(double mean, double sigma) {
    stddev = sigma;
    normal_dist = std::normal_distribution<double>(mean, sigma);
}

void RandomNumberGenerator::setup_uniform_dist(double mn, double mx) {
    uniform_dist = std::uniform_real_distribution<double>(mn, mx);
}
