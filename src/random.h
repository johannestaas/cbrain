#pragma once
#include <random>

class RandomNumberGenerator {
    public:
        RandomNumberGenerator();
        ~RandomNumberGenerator();
        double uniform();
        double normal();
        void setup_uniform_dist(double, double);
        void setup_normal_dist(double, double);
        double stddev;
    private:
        std::default_random_engine generator;
        std::normal_distribution<double> normal_dist;
        std::uniform_real_distribution<double> uniform_dist;
};
