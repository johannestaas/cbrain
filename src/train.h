#pragma once
#include <vector>
#include <memory>
#include <string>
#include <utility>
#include "datadir.h"
#include "neural_net.h"

typedef std::vector<std::vector<double>> IOPair;
typedef std::pair<std::string, std::string> IOPathPair;
typedef std::vector<IOPathPair> IOPathPairV;
typedef std::pair<std::vector<double>, std::vector<double>> IOVPair;
typedef std::pair<std::string, IOVPair> NamedIOPair;
typedef std::vector<NamedIOPair> NamedIOPairV;

class TrainingData {
    public:
        void set_input_dir(std::string);
        void set_output_dir(std::string);
        IOPathPairV scan_dirs(std::string, std::string);
        void set_training_sample(IOPathPairV);
        void set_fitness_sample(IOPathPairV);
        void preload_training_samples(); 
        NamedIOPair sample_at(int i) { return cached_input.at(i); };
        int sample_size() {return int(cached_input.size()); };
        NamedIOPair next_training_sample();
        NamedIOPair next_fitness_sample();
        void reset_training() { training_index = 0; };
        void reset_fitness() { fitness_index = 0; };
        bool training_empty() { return (training_index == int(training_sample.size())); };
        bool fitness_empty() { return (fitness_index == int(fitness_sample.size())); };
        void randomize_samples(IOPathPairV, float);
        void set_sizes(unsigned int, unsigned int);
        void add_pair(double[], double[]);
        unsigned int get_input_size();
        unsigned int get_output_size();
        unsigned int get_pair_size();
        IOPair get_pair(unsigned int);
        std::vector<double>* get_inputs(unsigned int);
        std::vector<double>& get_outputs(unsigned int);
        void create_sample(int);
        double calc_error(unsigned int, std::vector<double>&);
        double calc_error(std::vector<double>&, std::vector<double>&);
        double calc_error(std::vector<double>&);
        double calc_error_final(std::vector<double>&, std::vector<double>&);
        double calc_error_final(std::vector<double>&);
        void set_measurements(FFNet*, std::vector<double>&, std::vector<double>&);
    private:
        NamedIOPairV cached_input;
        std::vector<IOPair> io_pairs;
        int training_index;
        int fitness_index;
        unsigned int in_size;
        unsigned int out_size;
        IOPathPairV training_sample;
        IOPathPairV fitness_sample;
};
