#include <memory>
#include <deque>
#include <thread>
#include <iostream>
#include <algorithm>
#include <signal.h>
#include "train.h"
#include "genetic.h"
#include "random.h"

const int MAX_THREADS = std::thread::hardware_concurrency();

static bool CTRLC = false;

static void run_generation_thread(FFNet *net, Data *dataptr) {
    std::vector<double> errs = std::vector<double>();
    net->reset_measurements();
    for (int i=0; i<dataptr->sample_size(); i++) {
        net->reset();
        auto named_pair = dataptr->sample_at(i);
        auto pair = named_pair.second;
        net->activate(&pair.first);
        auto outputs = net->get_output();
        double err = dataptr->calc_error(pair.second, outputs);
        dataptr->set_measurements(net, pair.second, outputs);
        errs.push_back(err);
    }
    net->set_error(dataptr->calc_error(errs));
}

static void func_sigint_handler(int s) {
    if (CTRLC) {
        std::cout << "Force quit!" << std::endl;
        exit(1);
    }
    std::cout << "Ctrl-C caught, finishing generation..." << std::endl;
    CTRLC = true;
}

static void setup_sigint() {
    struct sigaction sigint_handler;
    sigint_handler.sa_handler = func_sigint_handler;
    sigemptyset(&sigint_handler.sa_mask);
    sigint_handler.sa_flags = 0;
    sigaction(SIGINT, &sigint_handler, NULL);
}

void GeneticGenerator::set_training_data(Data td) {
    data = td;
    data.preload_training_samples();
}

void GeneticGenerator::set_hidden_size(int ct, int layer_ct) {
    hidden_ct = ct;
    hidden_layer_ct = layer_ct;
}

FFNet GeneticGenerator::new_net() {
    FFNet net(&rng);
    net.add_input_layer(data.get_input_size());
    net.add_hidden_layers(hidden_ct, hidden_layer_ct);
    net.add_output_layer(data.get_output_size());
    return net;
}

void GeneticGenerator::setup_rng(double stddev) {
    rng = RandomNumberGenerator();
    rng.setup_uniform_dist(-1.0, 1.0);
    rng.setup_normal_dist(0.0, stddev);
}

void print_time(int gen, int gens, double total_seconds) {
    double avg = total_seconds / double(gen + 1);
    std::cout << "Average seconds: " << avg << std::endl;
    int left = int(double(gens - gen + 1) * avg);
    int days = left / (3600 * 24);
    int hours = (left / 3600) % 24;
    int minutes = (left / 60) % 60;
    int seconds = left % 60;
    std::cout << "time left estimate: " << days << ":" << hours << ":" << minutes << ":" << seconds << std::endl;
}

void GeneticGenerator::train(int generations, int initial_ct, int sample_ct, int children_ct, double stddev, std::string output_path) {
    setup_sigint();
    setup_rng(stddev);
    std::cout << "Maximum number of threads: " << MAX_THREADS << std::endl;
    std::cout << "Creating initial networks" << std::endl;
    for (int i=0; i<initial_ct; i++) {
        std::cout << "Creating " << i + 1 << " ..." << std::endl;
        nets.push_back(new_net());
    }
    double total_seconds = 0.0;
    for (int gen=0; gen<generations && CTRLC == false; gen++) {
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        std::cout << "Running generation " << gen << std::endl;
        run_generation();
        auto front_net = nets.front();
        front_net.print_measurements();
        if (front_net.false_positives + front_net.false_negatives == 0) {
            std::cout << "Converged on perfect results, breaking out at generation " << (gen + 1) << std::endl;
            break;
        }
        nets.erase(nets.begin() + sample_ct, nets.end());
        for (int net_i=0; net_i<sample_ct; net_i++) {
            for (int i=0; i<children_ct; i++) {
                FFNet new_net0 = FFNet(nets.at(net_i));
                new_net0.mutate();
                nets.push_back(new_net0);
            }
        }
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        int microsecs = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
        total_seconds += double(microsecs) / 1000000.0;
        print_time(gen, generations, total_seconds);
    }
    run_generation();
    nets.erase(nets.begin() + sample_ct, nets.end());
    auto best_net = nets.front();
    output_results(best_net);
    best_net.write(output_path);
}

void GeneticGenerator::run_generation() {
    volatile int net_i = 0;
    while (net_i < int(nets.size())) {
        int num_left = int(nets.size()) - net_i;
        int needed = num_left > MAX_THREADS ? MAX_THREADS : num_left;
        std::thread **threads = new std::thread*[needed];
        for (int i=0; i<needed; i++) {
            threads[i] = new std::thread(run_generation_thread, &nets.at(net_i), &data);
            net_i++;
        }
        for (int i=0; i<needed; i++) {
            threads[i]->join();
        }
        delete [] threads;
    }
    std::sort(nets.begin(), nets.end());
}

void GeneticGenerator::output_results(FFNet& best_net) {
    //best_net.debug();
    std::cout << "*** RESULTS ***" << std::endl;
    data.reset_training();
    data.reset_fitness();
    std::vector<double> errs;
    best_net.reset_measurements();
    /*
    while (!data.training_empty()) {
        best_net.reset();
        auto io_pair = data.next_training_sample();
        auto pair = io_pair.second;
        best_net.activate(&pair.first);
        auto outputs = best_net.get_output();
        double err = data.calc_error_final(pair.second, outputs);
        data.set_measurements(&best_net, pair.second, outputs);
        std::cout << io_pair.first << " training error " << errs.size() << ":\t" << err << std::endl;
        errs.push_back(err);
    }
    */
    while (!data.fitness_empty()) {
        best_net.reset();
        auto io_pair = data.next_fitness_sample();
        auto pair = io_pair.second;
        best_net.activate(&pair.first);
        auto outputs = best_net.get_output();
        double err = data.calc_error_final(pair.second, outputs);
        data.set_measurements(&best_net, pair.second, outputs);
        std::cout << io_pair.first << " fitness error " << errs.size() << ":\t" << err << std::endl;
        errs.push_back(err); 
    }
    double total_err = data.calc_error_final(errs);
    std::cout << "--------------------" << std::endl;
    std::cout << "Total Error: " << total_err << std::endl;
    best_net.print_measurements();
}
