#include <memory>
#include <iostream>
#include <string>
#include <unistd.h>
#include <getopt.h>
#include "cbrain.h"
#include "train.h"
#include "genetic.h"

extern char *optarg;
extern int optind, opterr, optopt;

int main(int argc, char* argv[]) {
    int opt;
    int generations = 50;
    float stddev = 0.05;
    float training_proportion = 0.25;
    int input_size = 2;
    int output_size = 1;
    int hidden_ct = 3;
    int hidden_layer_ct = 3;
    int initial_ct = 50;
    int reduce_amt = 10;
    int children_amt = 4;
    std::string output_path;
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " [-g NUM_GENS] [-t TRAINING_PROPORTION] [-i INPUT_DIR] [-o OUTPUT_DIR] [-s INPUT_SIZE] [-S OUTPUT_SIZE] [-h HIDDEN_LAYER_WIDTH] [-H HIDDEN_LAYER_COUNT] [-d STDDEV_MUTATION]" << std::endl;
        return 1;
    }
    std::vector<std::string> input_dirs;
    std::vector<std::string> output_dirs;
    while ((opt = getopt(argc, argv, "g:t:i:o:s:S:h:H:d:O:I:c:r:")) != -1) {
        switch (opt) {
            case 'g':
                generations = atoi(optarg);
                break;
            case 't':
                training_proportion = atof(optarg);
                break;
            case 'i':
                input_dirs.push_back(std::string(optarg));
                break;
            case 'o':
                output_dirs.push_back(std::string(optarg));
                break;
            case 's':
                input_size = atoi(optarg);
                break;
            case 'S':
                output_size = atoi(optarg);
                break;
            case 'h':
                hidden_ct = atoi(optarg);
                break;
            case 'H':
                hidden_layer_ct = atoi(optarg);
                break;
            case 'd':
                stddev = atof(optarg);
                break;
            case 'O':
                output_path = std::string(optarg);
                break;
            case 'I':
                initial_ct = atoi(optarg);
                break;
            case 'r':
                reduce_amt = atoi(optarg);
                break;
            case 'c':
                children_amt = atoi(optarg);
                break;
            default:
                std::cerr << "Usage: " << argv[0] << " [-g NUM_GENS] [-t TRAINING_PROPORTION] [-i INPUT_DIR] [-o OUTPUT_DIR] [-s INPUT_SIZE] [-S OUTPUT_SIZE] [-h HIDDEN_LAYER_WIDTH] [-H HIDDEN_LAYER_COUNT] [-d STDDEV_MUTATION]" << std::endl;
                return 1;
        }
    }
    auto td = TrainingData();
    for (int i=0; i<int(input_dirs.size()); i++) {
        auto pairs = td.scan_dirs(input_dirs.at(i), output_dirs.at(i));
        td.randomize_samples(pairs, double(training_proportion));
    }
    td.set_sizes(input_size, output_size);
    auto gen = GeneticGenerator();
    // 3 neurons by 3 hidden layers, tanh func
    gen.set_hidden_size(hidden_ct, hidden_layer_ct);
    gen.set_training_data(td);
    // train args are:
    // int generations, int initial_ct, int reduce_to_after_each_gen, int clones_per_sample
    gen.train(generations, initial_ct, reduce_amt, children_amt, double(stddev), output_path);
    return 0;
}
